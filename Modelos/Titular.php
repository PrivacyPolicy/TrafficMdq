<?php namespace Modelos;

class Titular{

	protected $nombre;
    protected $apellido;
	protected $domicilio;
	protected $email;
    protected $edad;
	protected $dni;
	protected $fechaNac;

	public function __construct($dni = 0, $nombre = '', $apellido = '', $email = '', $domicilio ='', $edad = '', $fechaNac = '')
	{
        $this->nombre = $nombre;
		$this->apellido = $apellido;
		$this->domicilio = $domicilio;
		$this->email = $email;
		$this->edad = $edad;
		$this->dni = $dni;
		$this->fechaNac = $fechaNac;
	}

    public function setNombre($nombre){
        $this->nombre=$nombre;
    }
   
    public function setApellido($apellido){
        $this->apellido=$apellido;
    }

    public function setDireccion($domicilio){
        $this->domicilio=$domicilio;
    }

    public function setEmail($email){
        $this->email=$email;
    }

    public function setEdad($edad){
        $this->edad=$edad;
    }

    public function setDni($dni){
        $this->dni=$dni;
    }

    public function setFechaNac($fechaNac){
    	$this->fechaNac=$fechaNac;
    }

	public function getNombre()
	{
		return $this->nombre;
	}

	public function getApellido()
	{
		return $this->apellido;
	}

	public function getDomicilio()
	{
		return $this->domicilio;
	}

	public function getEmail()
	{
		return $this->email;
	}

    public function getEdad()
	{
		return $this->edad;
	}

	public function getDni()
	{
		return $this->dni;
	}

	public function getFechaNac()
	{
		return $this->fechaNac;
	}

    public function getId()
	{
		return 1;
	}
}