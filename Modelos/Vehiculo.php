<?php namespace Modelos;

class Vehiculo 
{
	protected $dominio;
    protected $marca;
	protected $modelo;
	protected $fechaFab;
	protected $dni;
    
	/**
	 * Constructor
	 * 
	 * @param string  $nombre    Nombre de la persona
	 * @param string  $direccion Direccion de la persona
	 * @param integer $edad      Edad de la persona
	 */
    
	public function __construct($dominio = '', $marca = '', $modelo = '', $fechaFab = 0, $dni = 0)
	{
		$this->dominio = $dominio;
        $this->marca = $marca;
		$this->modelo = $modelo;
		$this->fechaFab = $fechaFab;
		$this->dni = $dni;
        
	}

	/**
	 * Devuelve el nombre de la persona
	 * 
	 * @return String Nombre devuelto
	 */
    
    public function setDominio($dominio){
        $this->dominio=$dominio;
    }
    
    public function setMarca($marca){
        $this->marca=$marca;
    }
    
    public function setModelo($modelo){
        $this->modelo=$modelo;
    }
    
    public function setFechaFab($fechaFab){
        $this->fechaFab=$$fechaFab;
    }
    
	public function getDominio()
	{
		return $this->dominio;
	}
    
	public function getMarca()
	{
		return $this->marca;
	}
    
    
	public function getModelo()
	{
		return $this->modelo;
	}
    
	public function getFechaFab()
	{
		return $this->fechaFab;
	}

	public function getDni()
	{
		return $this->dni;
	}

    public function getId()
	{
		return 1;
	}
    
	/**
	 * Listado de propiedades a serializar
	 * 
	 * @return Array Propiedades serializadas
	 */
    
    public function jsonSerialize() {
        return [
            'dominio' => $this->dominio,
            'marca' => $this->marca,
            'modelo' => $this->modelo,
            'fechaFab' => $this->fechaFab,
            'dni' => $this->dni,
            
        ];
    }
}