<?php namespace Modelos;

class Multa 
{
	protected $dni;
	protected $importe;
	protected $fecha;
	protected $dominio;
	protected $detalle;
    
	/**
	 * Constructor
	 * 
	 * @param string  $nombre    Nombre de la persona
	 * @param string  $direccion Direccion de la persona
	 * @param integer $edad      Edad de la persona
	 */
    
	public function __construct($dni = 0, $importe = '', $fecha = 0, $dominio = '', $detalle = '')
	{
		$this->dni = $dni;
		$this->importe = $importe;
		$this->fecha = $fecha;
		$this->dominio = $dominio;
		$this->detalle = $detalle;
        
	}

	/**
	 * Devuelve el nombre de la persona
	 * 
	 * @return String Nombre devuelto
	 */
    
    public function setDni($dni){
        $this->dni=$dni;
    }

    public function setImporte($importe){
        $this->importe=$importe;
    }
    
    public function setFecha($fecha){
        $this->fecha=$fecha;
    }

    public function setDominio($dominio){
        $this->dominio=$dominio;
    }

    public function setPago($detalle){
        $this->detalle=$detalle;
    }
    
	public function getDni()
	{
		return $this->dni;
	}
    
	public function getImporte()
	{
		return $this->importe;
	}
    
	public function getFecha()
	{
		return $this->fecha;
	}

	public function getDominio()
	{
		return $this->dominio;
	}

	public function getDetalle()
	{
		return $this->detalle;
	}

    public function getId()
	{
		return 1;
	}
    
	/**
	 * Listado de propiedades a serializar
	 * 
	 * @return Array Propiedades serializadas
	 */
}