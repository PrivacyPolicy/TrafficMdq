<?php namespace Modelos;

class Usuario //implements JsonSerializable
{
	protected $dni;
    protected $pass;
	protected $rol;

	public function __construct($dni = 0, $pass = '', $rol = '')
	{
        $this->dni = $dni;
        $this->pass = $pass;
		$this->rol = $rol;
	}

	
    public function setDni($dni){
        $this->dni=$dni;
    }
    
    public function setPass($pass){
        $this->pass=$pass;
    }
   
    public function setRol($rol){
        $this->rol=$rol;
    }

	public function getDni()
	{
		return $this->dni;
	}

	public function getPass()
	{
		return $this->pass;
	}

    public function getRol()
	{
		return $this->rol;
	}

    public function getId()
	{
		return 1;
	}

	/**
	 * Listado de propiedades a serializar
	 * 
	 * @return Array Propiedades serializadas
	 */
    public function jsonSerialize() {
        return [
            'nombre' => $this->nombre,
            'apellido' => $this->apellido,
            'fechanacimiento' => $this->fechanacimiento,
            'direccion' => $this->direccion,
            'edad' => $this->edad,
            'dni' => $this->dni
        ];
    }
}