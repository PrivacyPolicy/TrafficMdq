<!DOCTYPE html>
<html lang="en">

<head>

  <title>Sistema de Multas - Index de Administrador</title>

  <!-- Bootstrap Core CSS -->


  <!-- Custom CSS -->


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

      </head>

      <body>

        <div id="wrapper">

          <!-- Sidebar -->
          <div id="sidebar-wrapper" class="divider-right">
            <ul class="sidebar-nav text-center"  id="holder">
              <li class="sidebar-brand linear" id="">
                <a href="<?=FRONT?>Login/vistaPpalAdmin" class="">Marca

                </a>
              </li>
              <li>
                <a href="<?=FRONT?>Titulares/vistaTitulares" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Titulares"><i class="fa fa-users " aria-hidden="true"></i></a>
              </li>
              <li>
                <a href="<?=FRONT?>Vehiculos/vistaVehiculos" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Vehiculos"><i class="fa fa-car " aria-hidden="true"></i></a>
              </li>
              <li class="linear-margin">
                <a href="<?=FRONT?>Multas/vistaMultas" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Multas"><i class="fa fa-archive " aria-hidden="true"></i></a>
              </li>
              <li class="bottom-bar linear-before-margin">
                
              </li>
              
            </ul>
          </div>
          <!-- /#sidebar-wrapper -->

          <!-- Page Content -->
          <div id="page-content-wrapper">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <a href="#menu-toggle" class="btn btn-sm btn-shadow blue" id="menu-toggle" data-container="body" data-toggle="tooltip" data-placement="right" title="Toggle-Bar"><i class="fa fa-bars" aria-hidden="true"></i></a>
                  <a href="<?=FRONT?>Login/cerrarSesion" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="left" title="Sign-out"><i class="fa fa-sign-out " aria-hidden="true"></i></a>
                </div>
              </div>

              <div class="row top-margin">
                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Vehiculos</h1>
                      <p class="text-grey linear-margin">Resultado de la busqueda</p>

                      <h3 class="text-grey">Contenido</h3>
                      <p class="text-grey">
                        <?php
                        //echo $flag;
                        if($flag==3){
                          ?>
                          <p class="text-grey">Vehiculo Ingresado Correctamente</p>

                          <?php
                          if($unicoVehiculo!=NULL){
                            foreach($unicoVehiculo as $key => $value){   
                              ?>              
                              <table class="table text-grey" id="vertical-1">
                                <tr>
                                  <th>Dominio</th>
                                  <td><?php $dominio=$value['Dominio']; echo strtoupper($dominio); ?></td>
                                </tr>
                                <tr>
                                  <th>Dni</th>
                                  <td><?php echo $value['Dni']; ?></td>
                                </tr>
                                <tr>
                                  <th>FechaFeb</th>
                                  <td><?php echo $value['FechaFab']; ?></td>
                                </tr>
                                <tr>
                                  <th>Modelo</th>
                                  <td><?php echo $value['Modelo']; ?></td>
                                </tr>
                                <tr>
                                  <th>Marca</th>
                                  <td><?php echo $value['Marca']; ?></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-md-6">
                                  <form method="POST" action="<?=FRONT?>Vehiculos/borrarVehiculo">

                                <input type="hidden" name="dominio" value="<?php echo $value['Dominio']; ?>"/>
                                <button class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Eliminar"><i class="fa fa-trash " aria-hidden="true"></i></button>

                              </form>
                                </div>
                                <div class="col-md-6">
                                  <a href="<?=FRONT?>Vehiculos/vistaVehiculos" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a>

                                </div>
                              </div>
                              
                              <?php
                            }
                          }
                          else{
                            echo 'Ese Vehiculo no existe';
                          }
                        }
                        if($flag==0 || $flag==6){
                          ?> <p class="text-grey">Hay <?php

                          echo $listadoCant['Cant'];

                          ?> Vehiculos ingresados en el sistema</p>

                          <a href="<?=FRONT?>Vehiculos/mostrarTodos" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Ver Todos"><i class="fa fa-university " aria-hidden="true"></i></a> <!--Boton Traer todos-->
                          <?php
                          if($flag==6){?>
                            <a href="<?=FRONT?>Vehiculos/vistaVehiculos" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a>
                          <?php 
                          }
                        }
                        else if($flag == 1){
                          ?>
                          <?php
                          if($resultado!=NULL){
                            foreach($resultado as $key => $value){   
                              ?>              
                              <table class="table text-grey" id="vertical-1">
                                <tr>
                                  <th>Dominio</th>
                                  <td><?php $dominio=$value['Dominio']; echo strtoupper($dominio); ?></td>
                                </tr>
                                <tr>
                                  <th>Dni</th>
                                  <td><?php echo $value['Dni']; ?></td>
                                </tr>
                                <tr>
                                  <th>FechaFeb</th>
                                  <td><?php echo $value['FechaFab']; ?></td>
                                </tr>
                                <tr>
                                  <th>Modelo</th>
                                  <td><?php echo $value['Modelo']; ?></td>
                                </tr>
                                <tr>
                                  <th>Marca</th>
                                  <td><?php echo $value['Marca']; ?></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-md-6">
                                  <form method="POST" action="<?=FRONT?>Vehiculos/borrarVehiculo">

                                <input type="hidden" name="dominio" value="<?php echo $value['Dominio']; ?>"/>
                                <button class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Eliminar"><i class="fa fa-trash " aria-hidden="true"></i></button>

                              </form>
                                </div>
                                <div class="col-md-6">
                                  <a href="<?=FRONT?>Vehiculos/vistaVehiculos" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a>

                                </div>
                              </div>
                               
                              <?php
                            }
                          }
                          else{
                            echo 'Ese Vehiculo no existe';
                          }
                        }
                        else if($flag == 2) {
                          echo 'Vehiculo borrado correctamente';
                          ?><a href="<?=FRONT?>Vehiculos/vistaVehiculos" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a>
                        <?php
                        }
                        else if($flag==4){
                          echo 'El dni no pertenece a un titular cargado en el sistema';
                        }
                        else if($flag==5){
                          echo 'Ya hay un vehiculo cargado con ese dominio';
                        }

                        
                        ?>
                      </p>
                    </div>
                  </div>
                </div>

                <?php if($flag==0 || $flag==1 || $flag==2 || $flag==4 || $flag==5){?>

                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Buscar</h1>
                      <p class="text-grey linear-margin">Cualquier Vehiculo en la Base</p>

                      <h3 class="text-grey">Ingresar Dominio</h3>
                      <form method="POST" action="<?=FRONT?>Vehiculos/buscarVehiculo">
                        <input type="text" id="dominio" class="form-control form-format" placeholder="Dominio" name="dominio">
                      </br>
                      <button class="btn btn-sm btn-shadow blue" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                    
                    <h3 class="text-grey">Ingresar Dni</h3>
                      <form method="POST" action="<?=FRONT?>Vehiculos/buscarVehiculoDni">
                        <input type="text" id="dni" class="form-control form-format" placeholder="Dni" name="dominio">
                      </br>
                      <button class="btn btn-sm btn-shadow blue" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-md-4">
                <div class="info-bubble-color">
                  <div class="text-padding">
                    <h1 class="text-grey">Agregar</h1>
                    <p class="text-grey linear-margin">Ingrese datos para un nuevo vehiculo</p>
                    <h3 class="text-grey">Campos obligatorios</h3>
                    <form method="POST" action="<?=FRONT?>Vehiculos/agregarVehiculo" class="">
                      <input type="text" id="dni" class="form-control form-format" placeholder="Dni" name="Dni" required>
                      <input type="text" id="dominio" class="form-control form-format" placeholder="Dominio" name="Dominio" required>
                      <input type="text" id="fechafab" class="form-control form-format" placeholder="Fabricacion yyyy-mm-dd" name="FechaFab" required>
                      <input type="text" id="marca" class="form-control form-format" placeholder="Marca" name="Marca" required>
                      <input type="text" id="modelo" class="form-control form-format" placeholder="Modelo" name="Modelo" required>
                    </br>
                    <button class="btn btn-sm btn-shadow blue" type="submit" data-toggle="tooltip" data-placement="right" title="Agregar"><i class="fa fa-plus" aria-hidden="true"></i></button>
                  </form>
                </div>
              </div>
            </div>
            <?php }
            else{?>
              <div class="col-md-8">
                <div class="info-bubble-color">
                  <div class="text-padding">
                    <h1 class="text-grey linear-margin">Lista completa de Vehiculos</h1>
                    <?php if($listado!=NULL){?>

                    <table class="table text-grey">
                    <thead> 

                      <tr>
                      <th>Dni</th>
                        <th>Dominio</th> 
                        <th>Fabricacion yyyy-mm-dd</th>
                        <th>Marca</th>
                        <th>Modelo</th> 
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                      
                        foreach($listado as $key => $value){     
                          ?>
                          <tr>
                            <th><?php echo $value['Dni']; ?></th>
                            <td><?php $dominio=$value['Dominio']; echo strtoupper($dominio); ?></td>
                            <td><?php echo $value['FechaFab']; ?></td>
                            <td><?php echo $value['Marca']; ?></td>
                            <td><?php echo $value['Modelo']; ?></td>
                            <td><form method="POST" action="<?=FRONT?>Vehiculos/borrarVehiculo">

                                <input type="hidden" name="dominio" value="<?php echo $value['Dominio']; ?>"/>
                                <button class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Eliminar"><i class="fa fa-trash " aria-hidden="true"></i></button>

                              </form></td>
                          </tr> <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

                  <?php 
                  }
                }?>
          </div>
        </div>
      </div>
      <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?=FRONT_ROOT?>/Vistas/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=FRONT_ROOT?>/Vistas/js/bootstrap.min.js"></script>

    <!-- Tooltips-->
    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
      });
    </script>
    <!-- Menu Toggle Script -->
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    </script>

    <!--Hace que el trigger de los tooltip sea como el hover-->
    <!--El tooltip no queda en el aire despues de clickear el boton hasta que haya otro click-->
    <script>
      $('[data-toggle="tooltip"]').tooltip({
        trigger : 'hover'
      })  
    </script>
    <!--Js para hacer linea entre elementos de una lista-->
        <!--<script>
        $('li:not(:last)').addClass('list-line');
      </scrip