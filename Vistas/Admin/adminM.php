<!DOCTYPE html>
<html lang="en">

<head>

  <title>Sistema de Multas - Index de Administrador</title>

  <!-- Bootstrap Core CSS -->


  <!-- Custom CSS -->


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

      </head>

      <body>

        <div id="wrapper">

          <!-- Sidebar -->
          <div id="sidebar-wrapper" class="divider-right">
            <ul class="sidebar-nav text-center"  id="holder">
              <li class="sidebar-brand linear" id="">
                <a href="<?=FRONT?>Login/vistaPpalAdmin" class="">Marca

                </a>
              </li>
              <li>
                <a href="<?=FRONT?>Titulares/vistaTitulares" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Titulares"><i class="fa fa-users " aria-hidden="true"></i></a>
              </li>
              <li>
                <a href="<?=FRONT?>Vehiculos/vistaVehiculos" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Vehiculos"><i class="fa fa-car " aria-hidden="true"></i></a>
              </li>
              <li class="linear-margin">
                <a href="<?=FRONT?>Multas/vistaMultas" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Multas"><i class="fa fa-archive " aria-hidden="true"></i></a>
              </li>
              <li class="bottom-bar linear-before-margin" >

              </li>
              
            </ul>
          </div>
          <!-- /#sidebar-wrapper -->

          <!-- Page Content -->
          <div id="page-content-wrapper">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <a href="#menu-toggle" class="btn btn-sm btn-shadow blue" id="menu-toggle" data-container="body" data-toggle="tooltip" data-placement="right" title="Toggle-Bar"><i class="fa fa-bars" aria-hidden="true"></i></a>
                  <a href="<?=FRONT?>Login/cerrarSesion" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="left" title="Sign-out"><i class="fa fa-sign-out " aria-hidden="true"></i></a>
                </div>
              </div>

              <div class="row top-margin">
                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Multas</h1>
                      <p class="text-grey linear-margin">Resultado de una busqueda</p>

                      <h3 class="text-grey">Datos</h3>
                      <p class="text-grey">
                        <?php

                        /* Hace referencia a las funciones vistaTitulares y buscarTitular en las que si $flag tiene el valor '0' 
                         * se correra vistaTitulares y entrara en el if para mostrar un resumen.
                         * Si es '1' entrara en buscarTitular en donde traera un titular de la base al comenzar una busqueda, entrando en
                         * el else para mostrar los datos de dicho titular*/


                        /* Resultado de vistaTitulares($flag=0) y mostrarTodos($flag=5)*/
                        if($flag==0 || $flag==3 || $flag==5){
                          ?> <p class="text-grey">Hay <?php

                          echo $listadoCant['Cant'];

                          ?> Multas ingresadas en el sistema</p>

                          <a href="<?=FRONT?>Multas/mostrarTodos" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Ver Todos"><i class="fa fa-university " aria-hidden="true"></i></a> <!--Boton Traer todos-->
                          <?php
                          if($flag==5 || $flag==3){?>
                            <a href="<?=FRONT?>Multas/vistaMultas" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a>
                          <?php 
                          }
                        }

                        /* Resultado de buscarTitular*/
                        else if($flag==1){
                          ?>
                          
                          <?php
                          if($listado!=NULL){
                            foreach($listado as $key => $value){                 
                              ?>
                              <table class="table text-grey" id="vertical-1">

                                <tr>
                                  <th>Dni:</th>
                                  <td><?php echo $value['Dni']; ?></td>
                                </tr>
                                <tr>
                                  <th>Importe:</th>
                                  <td><?php echo $value['Importe']; ?></td>
                                </tr>
                                <tr>
                                  <th>Fecha:</th>
                                  <td><?php echo $value['Fecha']; ?></td>
                                </tr>
                                <tr>
                                  <th>Dominio:</th>
                                  <td><?php $dominio=$value['Dominio']; echo strtoupper($dominio); ?></td>
                                </tr>
                                <tr>
                                  <th>Detalle:</th>
                                  <td><?php echo $value['Detalle']; ?></td>
                                </tr>

                              </table>
                            <div class="row">
                                  <div class="col-md-6">
                                    <form method="POST" action="<?=FRONT?>Multas/borrarMulta">

                                <input type="hidden" name="Dni" value="<?php echo $value['IdMulta']; ?>"/>
                                <button class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Eliminar"><i class="fa fa-trash " aria-hidden="true"></i></button>

                              </form>
                                  </div>
                                  <div class="col-md-6">
                                    <a href="<?=FRONT?>Multas/vistaMultas" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a>
                                  </div>
                                </div>      
                              <?php
                            }
                          }

                          else{
                            echo 'Esa Multa no existe';
                          }
                        }
                        /* Resultado de borrarTitular*/
                        else if($flag==2){
                          echo 'Multa borrada con exito';
                          ?><a href="<?=FRONT?>Multas/vistaMultas" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a><?php
                        }

                        else if($flag==3){
                          echo 'Multa creada con exito';
                          ?><a href="<?=FRONT?>Multas/vistaMultas" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a><?php
                        }

                        else if($flag==4){
                          echo 'Ese Dni y ese Dominio no corresponden en nuestro sistema';
                        }
                        ?>


                      </div>
                    </div>
                  </div>
                  <?php if($flag==0 || $flag==1 || $flag==2 || $flag==4){?> <!--Buscar y Agregar activos -->


                  <div class="col-xs-12 col-md-4">
                    <div class="info-bubble-color">
                      <div class="text-padding">
                        <h1 class="text-grey">Buscar</h1>
                        <p class="text-grey linear-margin">Cualquier Multa en la Base</p>

                        <h3 class="text-grey">Ingresar Dni</h3>
                        <form method="POST" action="<?=FRONT?>Multas/buscarMultaDni" class="">

                          <input type="text" id="dni" class="form-control form-format" placeholder="Dni" name="dni"  >
                        </br>
                        <button class="btn btn-sm btn-shadow blue" type="submit" data-toggle="tooltip" data-placement="right" title="Buscar"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>

                        <h3 class="text-grey">Ingresar Dominio</h3>
                        <form method="POST" action="<?=FRONT?>Multas/buscarMultaDom">
                          <input type="text" id="dominio" class="form-control form-format" placeholder="Dominio" name="dominio">
                        </br>
                        <button class="btn btn-sm btn-shadow blue" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                      </form>

                      
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Agregar</h1>
                      <p class="text-grey linear-margin">Ingresar datos para nueva Multa</p>

                      <h3 class="text-grey">Campos obligatorios</h3>
                      <form method="POST" action="<?=FRONT?>Multas/agregarMulta" class="">
                        <input type="text" id="dni" class="form-control form-format" placeholder="Dni" name="Dni" required >
                        <input type="text" id="importe" class="form-control form-format" placeholder="Importe" name="Importe" required >
                        <input type="text" id="fecha" class="form-control form-format" placeholder="Fecha" name="Fecha" required >
                        <input type="text" id="dominio" class="form-control form-format" placeholder="Dominio" name="Dominio" required >
                        <input type="text" id="detalle" class="form-control form-format" placeholder="Detalle" name="Detalle" required >
                      </br>
                      <button class="btn btn-sm btn-shadow blue" type="submit" data-toggle="tooltip" data-placement="right" title="Agregar"><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </form>
                  </div>
                </div>
              </div>
              <?php } 

              /*Se llama desde mostrarTitulares y luego de agregarTitular*/  
              else{?>
              <div class="col-md-8">
                <div class="info-bubble-color">
                  <div class="text-padding">
                    <h1 class="text-grey linear-margin">Lista completa de Multas</h1>
                    <?php if($listado!=NULL){?>

                    <table class="table text-grey">
                    <thead> 

                      <tr>
                        <th>Dni</th> 
                        <th>Importe</th>
                        <th>Fecha</th>
                        <th>Dominio</th> 
                        <th>Detalle</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                      
                        foreach($listado as $key => $value){     
                          ?>
                          <tr>
                            <th><?php echo $value['Dni']; ?></th>
                            <th><span>$</span><?php echo $value['Importe']; ?></th>
                            <td><?php echo $value['Fecha']; ?></td>
                            <td><?php $dominio=$value['Dominio']; echo strtoupper($dominio); ?></td>
                            <td><?php echo $value['Detalle']; ?></td>
                            <td><form method="POST" action="<?=FRONT?>Multas/borrarMulta">

                                <input type="hidden" name="multa" value="<?php echo $value['IdMulta']; ?>"/>
                                <button class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Eliminar"><i class="fa fa-trash " aria-hidden="true"></i></button>

                              </form></td>
                          </tr> <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

                  <?php 
                  }
                }?>
                </div>
              </div>
            </div>
            <!-- /#page-content-wrapper -->
          </div>
          <!-- /#wrapper -->

          <!-- jQuery -->
          <script src="<?=FRONT_ROOT?>/Vistas/js/jquery.js"></script>

          <!-- Bootstrap Core JavaScript -->
          <script src="<?=FRONT_ROOT?>/Vistas/js/bootstrap.min.js"></script>

          <!-- Tooltips-->
          <script>
            $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();   
            });
          </script>
          <!-- Menu Toggle Script -->
          <script>
            $("#menu-toggle").click(function(e) {
              e.preventDefault();
              $("#wrapper").toggleClass("toggled");
            });
          </script>

          <!--Hace que el trigger de los tooltip sea como el hover-->
          <!--El tooltip no queda en el aire despues de clickear el boton hasta que haya otro click-->
          <script>
            $('[data-toggle="tooltip"]').tooltip({
              trigger : 'hover'
            })  
          </script>
          <!--Js para hacer linea entre elementos de una lista-->
        <!--<script>
        $('li:not(:last)').addClass('list-line');
      </script>-->

      <script>
        $(function () {

          $('#dni').data('holder', $('#dni').attr('placeholder'));

          $('#dni').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#dni').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#importe').data('holder', $('#importe').attr('placeholder'));

          $('#importe').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#importe').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#fecha').data('holder', $('#fecha').attr('placeholder'));

          $('#fecha').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#fecha').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#dominio').data('holder', $('#dominio').attr('placeholder'));

          $('#dominio').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#dominio').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });

        $(function () {

          $('#detalle').data('holder', $('#detalle').attr('placeholder'));

          $('#detalle').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#detalle').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        

       
      </script>


    </body>

    </html>