<!DOCTYPE html>
<html lang="en">

<head>

  <title>Sistema de Multas - Index de Administrador</title>

  <!-- Bootstrap Core CSS -->


  <!-- Custom CSS -->


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

      </head>

      <body>

        <div id="wrapper">

          <!-- Sidebar -->
          <div id="sidebar-wrapper" class="divider-right">
            <ul class="sidebar-nav text-center"  id="holder">
              <li class="sidebar-brand linear" id="">
                <a href="<?=FRONT?>Login/vistaPpalAdmin" class="">Marca

                </a>
              </li>
              <li>
                <a href="<?=FRONT?>Titulares/vistaTitulares" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Titulares"><i class="fa fa-users " aria-hidden="true"></i></a>
              </li>
              <li>
                <a href="<?=FRONT?>Vehiculos/vistaVehiculos" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Vehiculos"><i class="fa fa-car " aria-hidden="true"></i></a>
              </li>
              <li class="linear-margin">
                <a href="<?=FRONT?>Multas/vistaMultas" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Multas"><i class="fa fa-archive " aria-hidden="true"></i></a>
              </li>
              <li class="bottom-bar linear-before-margin">
                
              </li>
              
            </ul>
          </div>
          <!-- /#sidebar-wrapper -->

          <!-- Page Content -->
          <div id="page-content-wrapper">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <a href="#menu-toggle" class="btn btn-sm btn-shadow blue" id="menu-toggle" data-container="body" data-toggle="tooltip" data-placement="right" title="Toggle-Bar"><i class="fa fa-bars" aria-hidden="true"></i></a>
                  <a href="<?=FRONT?>Login/cerrarSesion" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="left" title="Sign-out"><i class="fa fa-sign-out " aria-hidden="true"></i></a>
                </div>
              </div>

              <div class="row top-margin">
                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Titulares</h1>
                      <p class="text-grey linear-margin">Resumen de Titulares</p>
                    
                      <h3 class="text-grey">Datos</h3>
                      <p class="text-grey">Hay <?php
                          echo $listadoCantT['Cant'];
                          ?> Titulares ingresados en el sistema</p>
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Vehiculos</h1>
                      <p class="text-grey linear-margin">Resumen de Vehiculos</p>
                    
                      <h3 class="text-grey">Datos</h3>
                      <p class="text-grey">Hay <?php
                          echo $listadoCantV['Cant'];
                          ?> Vehiculos ingresados en el sistema</p>
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Multas</h1>
                      <p class="text-grey linear-margin">Resumen de Multas</p>
                      
                      <h3 class="text-grey">Datos</h3>
                      <p class="text-grey">Hay <?php
                          echo $listadoCantM['Cant'];
                          ?> Multas ingresadas al sistema</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /#page-content-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="<?=FRONT_ROOT?>/Vistas/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?=FRONT_ROOT?>/Vistas/js/bootstrap.min.js"></script>

        <!-- Tooltips-->
        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
          });
        </script>
        <!-- Menu Toggle Script -->
        <script>
          $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
          });
        </script>
        <!--Hace que el trigger de los tooltip sea como el hover-->
        <!--El tooltip no queda en el aire despues de clickear el boton hasta que haya otro click-->
        <script>
          $('[data-toggle="tooltip"]').tooltip({
          trigger : 'hover'
          })  
        </script>
        
        <!--Js para hacer linea entre elementos de una lista-->
        <!--<script>
        $('li:not(:last)').addClass('list-line');
      </script>-->

    </body>

    </html>