<!DOCTYPE html>
<html lang="en">

<head>

  <title>Sistema de Multas - Index de Administrador</title>

  <!-- Bootstrap Core CSS -->


  <!-- Custom CSS -->


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

      </head>

      <body>

        <div id="wrapper">

          <!-- Sidebar -->
          <div id="sidebar-wrapper" class="divider-right">
            <ul class="sidebar-nav text-center"  id="holder">
              <li class="sidebar-brand linear" id="">
                <a href="<?=FRONT?>Login/vistaPpalAdmin" class="">Marca

                </a>
              </li>
              <li>
                <a href="<?=FRONT?>Titulares/vistaTitulares" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Titulares"><i class="fa fa-users " aria-hidden="true"></i></a>
              </li>
              <li>
                <a href="<?=FRONT?>Vehiculos/vistaVehiculos" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Vehiculos"><i class="fa fa-car " aria-hidden="true"></i></a>
              </li>
              <li class="linear-margin">
                <a href="<?=FRONT?>Multas/vistaMultas" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Multas"><i class="fa fa-archive " aria-hidden="true"></i></a>
              </li>
              <li class="bottom-bar linear-before-margin" >

              </li>
              
            </ul>
          </div>
          <!-- /#sidebar-wrapper -->

          <!-- Page Content -->
          <div id="page-content-wrapper">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <a href="#menu-toggle" class="btn btn-sm btn-shadow blue" id="menu-toggle" data-container="body" data-toggle="tooltip" data-placement="right" title="Toggle-Bar"><i class="fa fa-bars" aria-hidden="true"></i></a>
                  <a href="<?=FRONT?>Login/cerrarSesion" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="left" title="Sign-out"><i class="fa fa-sign-out " aria-hidden="true"></i></a>
                </div>
              </div>

              <div class="row top-margin">
                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Titular</h1>
                      <p class="text-grey linear-margin">Resultado de una busqueda</p>

                      <h3 class="text-grey">Datos</h3>
                      <p class="text-grey">
                        <?php

                        /* Hace referencia a las funciones vistaTitulares y buscarTitular en las que si $flag tiene el valor '0' 
                         * se correra vistaTitulares y entrara en el if para mostrar un resumen.
                         * Si es '1' entrara en buscarTitular en donde traera un titular de la base al comenzar una busqueda, entrando en
                         * el else para mostrar los datos de dicho titular*/


                        /* Resultado de vistaTitulares($flag=0) y mostrarTodos($flag=5)*/
                        if($flag==0 || $flag==3 || $flag==5){
                          ?> <p class="text-grey">Hay <?php

                          echo $listadoCant['Cant'];

                          ?> Titulares ingresados en el sistema</p>

                          <a href="<?=FRONT?>Titulares/mostrarTodos" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Ver Todos"><i class="fa fa-university " aria-hidden="true"></i></a> <!--Boton Traer todos-->
                          <?php
                          if($flag==5 || $flag==3){?>
                            <a href="<?=FRONT?>Titulares/vistaTitulares" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a>
                          <?php 
                          }
                        }

                        /* Resultado de buscarTitular*/
                        else if($flag==1){
                          ?>
                          
                          <?php
                          if($listado!=NULL){
                            foreach($listado as $key => $value){                 
                              ?>
                              <table class="table text-grey" id="vertical-1">

                                <tr>
                                  <th>Nombre:</th>
                                  <td><?php echo $value['Nombre']; ?></td>
                                </tr>
                                <tr>
                                  <th>Apellido:</th>
                                  <td><?php echo $value['Apellido']; ?></td>
                                </tr>
                                <tr>
                                  <th>Dni:</th>
                                  <td><?php echo $value['Dni']; ?></td>
                                </tr>
                                <tr>
                                  <th>E-mail:</th>
                                  <td><?php echo $value['Email']; ?></td>
                                </tr>
                                <tr>
                                  <th>Domicilio:</th>
                                  <td><?php echo $value['Domicilio']; ?></td>
                                </tr>
                                <tr>
                                  <th>Edad:</th>
                                  <td><?php echo $value['Edad']; ?></td>
                                </tr>
                                <tr>
                                  <th>Fecha de Nacimiento:</th>
                                  <td><?php echo $value['FechaNac']; ?></td>
                                </tr>

                              </table>
                                <div class="row">
                                  <div class="col-md-6">
                                    <form method="POST" action="<?=FRONT?>Titulares/borrarTitular">

                                <input type="hidden" name="Dni" value="<?php echo $value['Dni']; ?>"/>
                                <button class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Eliminar"><i class="fa fa-trash " aria-hidden="true"></i></button>

                              </form>
                                  </div>
                                  <div class="col-md-6">
                                    <a href="<?=FRONT?>Titulares/vistaTitulares" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a>
                                  </div>
                                </div>
                              

                              
                              <?php
                            }
                          }

                          else{
                            echo 'Ese titular no existe';
                          }
                        }
                        /* Resultado de borrarTitular*/
                        else if($flag==2){
                          echo 'Titular borrado con exito';
                          ?><a href="<?=FRONT?>Titulares/vistaTitulares" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="right" title="Volver"><i class="fa fa-arrow-left " aria-hidden="true"></i></a><?php
                        }

                        else if($flag==3){
                          echo 'Titular creado con exito';
                        }

                        else if($flag==4){
                          echo 'Ese Titular ya existe';
                        }
                        ?>


                      </div>
                    </div>
                  </div>
                  <?php if($flag==0 || $flag==1 || $flag==2 || $flag==4){?> <!--Buscar y Agregar activos -->


                  <div class="col-xs-12 col-md-4">
                    <div class="info-bubble-color">
                      <div class="text-padding">
                        <h1 class="text-grey">Buscar</h1>
                        <p class="text-grey linear-margin">Cualquier Titular en la Base</p>

                        <h3 class="text-grey">Ingresar Dni</h3>
                        <form method="POST" action="<?=FRONT?>Titulares/buscarTitular" class="">

                          <input type="text" id="inputEmail" class="form-control form-format" placeholder="Dni" name="dni" required >
                        </br>
                        <button class="btn btn-sm btn-shadow blue" type="submit" data-toggle="tooltip" data-placement="right" title="Buscar"><i class="fa fa-search" aria-hidden="true"></i></button>

                      </form>
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-md-4">
                  <div class="info-bubble-color">
                    <div class="text-padding">
                      <h1 class="text-grey">Agregar</h1>
                      <p class="text-grey linear-margin">Ingresar datos para nuevo Titular</p>

                      <h3 class="text-grey">Campos obligatorios</h3>
                      <form method="POST" action="<?=FRONT?>Titulares/agregarTitular" class="">
                        <input type="text" id="dni" class="form-control form-format" placeholder="Dni" name="Dni" required >
                        <input type="text" id="nombre" class="form-control form-format" placeholder="Nombre" name="Nombre" required >
                        <input type="text" id="apellido" class="form-control form-format" placeholder="Apellido" name="Apellido" required >
                        <input type="text" id="email" class="form-control form-format" placeholder="Email" name="Email" required >
                        <input type="text" id="domicilio" class="form-control form-format" placeholder="Domicilio" name="Domicilio" required >
                        <input type="text" id="edad" class="form-control form-format" placeholder="Edad" name="Edad" required >
                        <input type="text" id="fechaNac" class="form-control form-format" placeholder="Nacimiento yyyy-mm-dd" name="FechaNac" required >
                      </br>
                      <button class="btn btn-sm btn-shadow blue" type="submit" data-toggle="tooltip" data-placement="right" title="Agregar"><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </form>
                  </div>
                </div>
              </div>
              <?php } 

              /*Se llama desde mostrarTitulares y luego de agregarTitular*/  
              else{?>
              <div class="col-md-8">
                <div class="info-bubble-color">
                  <div class="text-padding">
                    <h1 class="text-grey linear-margin">Lista completa de titulares</h1>
                    <?php if($listado!=NULL){?>

                    <table class="table text-grey">
                    <thead> 

                      <tr>
                      <th>Nombre</th>
                        <th>Apellido</th> 
                        <th>Dni</th>
                        <th>E-mail</th>
                        <th>Domicilio</th> 
                        <th>Edad</th>
                        <th>Fecha de Nacimiento</th> 
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                      
                        foreach($listado as $key => $value){     
                          ?>
                          <tr>
                            <th><?php echo $value['Nombre']; ?></th>
                            <th><?php echo $value['Apellido']; ?></th>
                            <td><?php echo $value['Dni']; ?></td>
                            <td><?php echo $value['Email']; ?></td>
                            <td><?php echo $value['Domicilio']; ?></td>
                            <td><?php echo $value['Edad']; ?></td>
                            <th><?php echo $value['FechaNac']; ?></th>
                            <td><form method="POST" action="<?=FRONT?>Titulares/borrarTitular">

                                <input type="hidden" name="Dni" value="<?php echo $value['Dni']; ?>"/>
                                <button class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Eliminar"><i class="fa fa-trash " aria-hidden="true"></i></button>

                              </form></td>
                          </tr> <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

                  <?php 
                  }
                }?>
                </div>
              </div>
            </div>
            <!-- /#page-content-wrapper -->
          </div>
          <!-- /#wrapper -->

          <!-- jQuery -->
          <script src="<?=FRONT_ROOT?>/Vistas/js/jquery.js"></script>

          <!-- Bootstrap Core JavaScript -->
          <script src="<?=FRONT_ROOT?>/Vistas/js/bootstrap.min.js"></script>

          <!-- Tooltips-->
          <script>
            $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();   
            });
          </script>
          <!-- Menu Toggle Script -->
          <script>
            $("#menu-toggle").click(function(e) {
              e.preventDefault();
              $("#wrapper").toggleClass("toggled");
            });
          </script>

          <!--Hace que el trigger de los tooltip sea como el hover-->
          <!--El tooltip no queda en el aire despues de clickear el boton hasta que haya otro click-->
          <script>
            $('[data-toggle="tooltip"]').tooltip({
              trigger : 'hover'
            })  
          </script>
          <!--Js para hacer linea entre elementos de una lista-->
        <!--<script>
        $('li:not(:last)').addClass('list-line');
      </script>-->

      <script>
        $(function () {

          $('#dni').data('holder', $('#dni').attr('placeholder'));

          $('#dni').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#dni').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#nombre').data('holder', $('#nombre').attr('placeholder'));

          $('#nombre').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#nombre').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#apellido').data('holder', $('#apellido').attr('placeholder'));

          $('#apellido').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#apellido').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#email').data('holder', $('#email').attr('placeholder'));

          $('#email').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#email').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });

        $(function () {

          $('#domicilio').data('holder', $('#domicilio').attr('placeholder'));

          $('#domicilio').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#domicilio').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#edad').data('holder', $('#edad').attr('placeholder'));

          $('#edad').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#edad').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#fechaNac').data('holder', $('#fechaNac').attr('placeholder'));

          $('#fechaNac').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#fechaNac').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
      </script>

    </body>

    </html>