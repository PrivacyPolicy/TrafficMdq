<!DOCTYPE html>
<html lang="en">

<head>

  <title>Mis vehiculos</title>

</head>

<body>

  <div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper" class="divider-right">
      <ul class="sidebar-nav text-center"  id="holder">
            <li class="sidebar-brand linear" id="">

              <a href="<?=FRONT?>Login/vistaPpalUser" class="">Marca

              </a>
            </li>
            <li>

              <a href="<?=FRONT?>Titulares/vistaTitularUser" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Titulares"><i class="fa fa-users " aria-hidden="true"></i></a>

            </li>
            <li>
              <a href="<?=FRONT?>Vehiculos/vistaVehiculoUser" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Vehiculos"><i class="fa fa-car " aria-hidden="true"></i></a>
            </li>

            <li class="linear-margin">
              <a href="<?=FRONT?>Multas/vistaMultaUser" class="btn btn-sm btn-shadow blue" data-container="body" data-toggle="tooltip" data-placement="right" title="Multas"><i class="fa fa-archive " aria-hidden="true"></i></a>
            </li>
            <li class="bottom-bar linear-before-margin" >

            </li>

          </ul>
      </div>
      <!-- /#sidebar-wrapper -->

      <!-- Page Content -->
      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <a href="#menu-toggle" class="btn btn-sm btn-shadow blue" id="menu-toggle" data-container="body" data-toggle="tooltip" data-placement="right" title="Toggle-Bar"><i class="fa fa-bars" aria-hidden="true"></i></a>
              <a href="<?=FRONT?>Login/cerrarSesion" class="btn btn-sm btn-shadow blue align-right" data-container="body" data-toggle="tooltip" data-placement="left" title="Sign-out"><i class="fa fa-sign-out " aria-hidden="true"></i></a>
            </div>
          </div>

          <div class="row top-margin">
            <div class="col-xs-12 col-md-8">
              <div class="info-bubble-color">
                <div class="text-padding">
                  <h1 class="text-grey">Vehiculos</h1>
                  <p class="text-grey linear-margin">Resultado de una busqueda</p>

                  <h3 class="text-grey">Datos Vehiculo</h3>
                  <p class="text-grey">
                    <?php

                        /* Hace referencia a las funciones vistaVehiculo y buscarVehiculo en las que si $flag tiene el valor '0' 
                         * se correra vistaVehiculo y entrara en el if para mostrar un resumen.
                         * Si es '1' entrara en buscarVehiculo en donde traera un vehiculo de la base al comenzar una busqueda, entrando en
                         * el else para mostrar los datos de dicho vehiculo*/

                        /* Resultado de buscarVehiculo*/
                        if($flag==0){

                          ?>
                          <table class="table text-grey">
                          <tr>
                            <th>Dominio</th>
                            <th>Dni</th> 
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Fecha de Fabricacion</th> 
                          </tr>
                          
                          <?php
                          if($listado!=NULL){
                            foreach($listado as $key => $value){     
                            //echo 'entro bien hasta aca3';    
                              //echo 'muestra1';
                            //    
                              ?>
                                <tr>
                                  <th><?php echo $value['Dominio']; ?></th>
                                  <td><?php echo $value['Dni']; ?></td>
                                  <td><?php echo $value['Marca']; ?></td>
                                  <td><?php echo $value['Modelo']; ?></td>
                                  <td><?php echo $value['FechaFab']; ?></td>
                                </tr>
                                <?php } ?>
                              </table>
                          <?php }
                          else{
                          echo 'No encontramos vehiculos cargados';
                        }
                      }
                        ?>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-md-4">
                </div>
                <div class="col-xs-12 col-md-4">
                </div>
              </div>
            </div>
          </div>
          <!-- /#page-content-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="<?=FRONT_ROOT?>/Vistas/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?=FRONT_ROOT?>/Vistas/js/bootstrap.min.js"></script>

        <!-- Tooltips-->
        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
          });
        </script>
        <!-- Menu Toggle Script -->
        <script>
          $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
          });
        </script>

        <!--Hace que el trigger de los tooltip sea como el hover-->
        <!--El tooltip no queda en el aire despues de clickear el boton hasta que haya otro click-->
        <script>
          $('[data-toggle="tooltip"]').tooltip({
            trigger : 'hover'
          })  
        </script>
        <!--Js para hacer linea entre elementos de una lista-->
        <!--<script>
        $('li:not(:last)').addClass('list-line');
      </script>-->

    </body>

    </html>