<!doctype html>
<html>

<head>
    <title>TrafficMDQ</title>
</head>

<body class="login-body" style="">
    <div class="container pad-lg-top " id="suppa-row">
        <div class="row" id="suppa-row">
            <div class="col-md-offset-3 col-md-6" id="login">
                <div class="info-bubble-color">
                <div class="text-padding">
                    <h1 class="text-center text-grey"><i class="fa fa-lock" id="lock-icon" aria-hidden="true"></i>TrafficMDQ</h1>
                    <!-- <p id="profile-name" class="profile-name-card"></p> -->

                    <form method="POST" action="<?=FRONT_ROOT?>Login/loginUsuario" class="form-signin">
                        
                        <input type="text" id="inputEmail" class="form-control form-format" placeholder="Dni" name="Dni" required>

                        <input type="password" id="inputPassword" class="form-control form-format" placeholder="Password" name="Pass" required>
                        <div id="remember" class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me"> <p class="text-grey">Remember me</p>
                            </label>
                        </div>
                        <button class="btn btn-md btn-shadow blue" type="submit" data-container="body" data-toggle="tooltip" data-placement="right" title="Sign-in">
                           <i class="fa fa-sign-in" aria-hidden="true"></i>
                        </button>

                    </form>
                    <!-- /form -->
                    <a href="#" class="text-grey">
                Forgot the password?
                    </a>
                </div>
                    
                    
                </div>
  
            </div>



        </div>
    </div>
    <!-- /container -->

    
    <!-- jQuery -->
    <script src="<?=FRONT_ROOT?>/Vistas/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=FRONT_ROOT?>/Vistas/js/bootstrap.min.js"></script>

    <!-- Tooltips-->
    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
      });
    </script>
    <script>
      $('[data-toggle="tooltip"]').tooltip({
        trigger : 'hover'
      })  
    </script>
    <script>
        $(function () {

          $('#inputEmail').data('holder', $('#inputEmail').attr('placeholder'));

          $('#inputEmail').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#inputEmail').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });


        });
        $(function () {

          $('#inputPassword').data('holder', $('#inputPassword').attr('placeholder'));

          $('#inputPassword').focusin(function () {
            $(this).attr('placeholder', '');
          });
          $('#inputPassword').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
          });
    </script>
   
</body>

</html>