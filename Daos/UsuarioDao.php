<?php namespace Daos;

	use \Daos\Conexion as Conexion;

	class UsuarioDao extends Conexion /*implements IDAO*/ {

		protected $tabla = "Usuarios";
        protected $listado;
        
        protected function mapear($gente)
	       {
		      $gente = is_array($gente) ? $gente : [];
		      $this->listado = array_map(function($p){
			     return new \Modelos\Usuario(isset($p['dni']), isset($p['pass']), isset($p['rol']));
		      }, $gente);
	       }
        
        
        
		public function traerUno($value) {
            
            $sql = "SELECT * FROM " . $this->tabla . " WHERE Dni=" . $value;
                

            // creo el objeto conexion
			$obj_pdo = new Conexion();

			// Conecto a la base de datos.
			$conexion = $obj_pdo->conectar();

			// Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
			$sentencia = $conexion->prepare($sql);

			// Ejecuto la sentencia.
			$sentencia->execute();
			//var_dump($sentencia->execute());
			
			while ($row = $sentencia->fetch()) {
				$array[] = $row;
				//var_dump($row);
            }
            if(!empty($array)){
				return $array;

			}

		}

		public function isValid($Dni, $Pass){
			$sql = "SELECT * FROM Usuarios WHERE Dni = $Dni and Pass = '$Pass' ";
			//$sql = "SELECT Rol FROM " . $this->tabla . " WHERE Dni=" . $Dni;
                //var_dump($sql);

            // creo el objeto conexion
			$obj_pdo = new Conexion();

			// Conecto a la base de datos.
			$conexion = $obj_pdo->conectar();

			// Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
			$sentencia = $conexion->prepare($sql);
			//var_dump($sentencia);

			// Ejecuto la sentencia.
			$sentencia->execute();
			while ($row = $sentencia->fetch()) {
				$array[] = $row;
            }
			if(!empty($array)){
				return $array;
			}
		}

		public function isAdmin($Dni){
			$sql = "SELECT * FROM Usuarios WHERE Dni = $Dni and Rol = 'admin' ";
			//$sql = "SELECT Rol FROM " . $this->tabla . " WHERE Dni=" . $Dni;
                //var_dump($sql);

            // creo el objeto conexion
			$obj_pdo = new Conexion();

			// Conecto a la base de datos.
			$conexion = $obj_pdo->conectar();

			// Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
			$sentencia = $conexion->prepare($sql);
			//var_dump($sentencia);

			// Ejecuto la sentencia.
			$sentencia->execute();
			while ($row = $sentencia->fetch()) {
				$array[] = $row;
            }
			if(!empty($array)){
				return $array;
			}
		}

		public function isUser($Dni){
			$sql = "SELECT * FROM Usuarios WHERE Dni = $Dni and Rol = 'User' ";
			//$sql = "SELECT Rol FROM " . $this->tabla . " WHERE Dni=" . $Dni;
                //var_dump($sql);

            // creo el objeto conexion
			$obj_pdo = new Conexion();

			// Conecto a la base de datos.
			$conexion = $obj_pdo->conectar();

			// Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
			$sentencia = $conexion->prepare($sql);
			//var_dump($sentencia);

			// Ejecuto la sentencia.
			$sentencia->execute();
			while ($row = $sentencia->fetch()) {
				$array[] = $row;
            }
			if(!empty($array)){
				return $array;
			}
		}
        

		public function traerTodos() {

			// Guardo como string la consulta sql
			$sql = "SELECT * FROM " . $this->tabla;


			// creo el objeto conexion
			$obj_pdo = new Conexion();


			// Conecto a la base de datos.
			$conexion = $obj_pdo->conectar();


			// Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
			$sentencia = $conexion->prepare($sql);


			// Ejecuto la sentencia.
			$sentencia->execute();


			while ($row = $sentencia->fetch()) {
				$array[] = $row;
            }
            
            if(!empty($array)){
				return $array;
			}

			/*if(!empty($array)) {
                $this->mapear($array);
                //$this->listado=$array;
            }
            return $this->listado;*/

		}

		public function actualizar($value) {}

		public function eliminar($value) {

			// Guardo como string la consulta sql
			$sql = "DELETE FROM " . $this->tabla . " WHERE id=" . $value;


			// creo el objeto conexion
			$obj_pdo = new Conexion();


			// Conecto a la base de datos.
			$conexion = $obj_pdo->conectar();


			// Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
			$sentencia = $conexion->prepare($sql);


			// Ejecuto la sentencia.
			$sentencia->execute();

		}

		public function agregar($nombre,$apellido,$fechanacimiento,$direccion,$edad, $dni) {

			// Guardo como string la consulta sql utilizando como values, marcadores de parámetros con nombre (:name) o signos de interrogación 
			//(?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada
			$sql = "INSERT INTO " . $this->tabla . " (nombre, apellido, fechanacimiento, direccion, edad, dni) VALUES 
			(:nombre, :apellido, :fechanacimiento, :direccion, :edad, :dni)";

			
			// creo el objeto conexion
			$obj_pdo = new Conexion();


			// Conecto a la base de datos.
			$conexion = $obj_pdo->conectar();


			// Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
			$sentencia = $conexion->prepare($sql);


			// Reemplazo los marcadores de parametro por los valores reales utilizando el método bindParam().
			$sentencia->bindParam(":nombre", $nombre);
            $sentencia->bindParam(":nombre", $apellido);
            $sentencia->bindParam(":nombre", $fechanacimiento);
            $sentencia->bindParam(":nombre", $direccion);
            $sentencia->bindParam(":nombre", $edad);
            $sentencia->bindParam(":nombre", $dni);
            
			

			// Ejecuto la sentencia.
			$sentencia->execute();

		}
	}

?>