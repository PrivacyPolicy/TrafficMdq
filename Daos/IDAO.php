<?php namespace Daos;

Interface IDAO
{
	public function agregar($dato);

	public function todas();

	public function traer($dato);

}