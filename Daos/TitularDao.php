<?php namespace Daos;

use \Daos\Conexion as Conexion;

class TitularDao extends Conexion /*implements IDAO*/{
	protected $tabla = "Titulares";
	protected $listado;

	protected function mapear($gente)
	{
		$gente = is_array($gente) ? $gente : [];
		$this->listado = array_map(function($p){
			return new \Modelos\Usuario(isset($p['dni']), isset($p['nombre']), isset($p['apellido']), isset($p['email']), isset($p['edad']));
		}, $gente);
	}

	public function agregar($objeto) {

			try{
				// Guardo como string la consulta sql utilizando como values, marcadores de parámetros con nombre (:name) o signos de interrogación 
			//(?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada
			/*$sql = "INSERT INTO " . $this->tabla . " (Dni, Nombre, Apellido, Email, Domicilio, Edad, FechaNac) VALUES 
			(:Dni, :Nombre, :Apellido, :Domicilio, :Edad, :FechaNac)";*/

			$Dni = $objeto->getDni();
			$Nombre = $objeto->getNombre();
			$Apellido = $objeto->getApellido();
			$Email = $objeto->getEmail();
			$Domicilio = $objeto->getDomicilio();
			$Edad = $objeto->getEdad();
			$FechaNac = $objeto->getFechaNac();

			$sql = "INSERT INTO $this->tabla  (Dni, Nombre, Apellido, Email, Domicilio, Edad, FechaNac)  VALUES ($Dni, '$Nombre', '$Apellido', '$Email', '$Domicilio', $Edad, '$FechaNac')";

			//$sql = "INSERT INTO " . $this->tabla . " VALUES (:dni, :nombre, :fechaNacimiento, :domicilio, :password)";
			//echo $sql;

			$obj_pdo = new Conexion();

			$conexion = $obj_pdo->conectar();

			$sentencia = $conexion->prepare($sql);

			/*$sentencia->bindParam(":Dni", $Dni);
			$sentencia->bindParam(":Nombre", $Nombre);
            $sentencia->bindParam(":Apellido", $Apellido);
            $sentencia->bindParam(":Email", $Email);
            $sentencia->bindParam(":Domicilio", $Domicilio);
            $sentencia->bindParam(":Edad", $Edad);
            $sentencia->bindParam(":FechaNac", $FechaNac);*/

            //var_dump($sentencia);

			// Ejecuto la sentencia.
            $sentencia->execute();
			}

            catch (Exception $ex) {
            	die("Error: " . $ex->getMessage());
            }

        }
        public function eliminar($value) {

			try{
				// Guardo como string la consulta sql
	        	$sql = "DELETE FROM " . $this->tabla . " WHERE Dni=" . $value;


	        	$obj_pdo = new Conexion();
	        	$conexion = $obj_pdo->conectar();
	        	$sentencia = $conexion->prepare($sql);
	        	$sentencia->execute();
	        	while ($row = $sentencia->fetch()) {
	        		$array[] = $row;
	        	}
			}

        	catch (Exception $ex) {
        		die("Error: " . $ex->getMessage());
        	}
        	if(!empty($array)){
        		return $array;
        	}

        }

        public function traerTodos() {

			try{
				// Guardo como string la consulta sql
	        	$sql = "SELECT * FROM " . $this->tabla;
	        	$obj_pdo = new Conexion();
	        	$conexion = $obj_pdo->conectar();
	        	$sentencia = $conexion->prepare($sql);
	        	$sentencia->execute();

	        	while ($row = $sentencia->fetch()) {
	        		$array[] = $row;
	        	}
			}

        	catch (Exception $ex) {
        		die("Error: " . $ex->getMessage());
        	}
        	if(!empty($array)){
        		return $array;
        	}
        }

        public function traerCant(){
        	
        	try{
        		$sql = "SELECT count(*) as Cant FROM " . $this->tabla;
				//var_dump($sql);
	        	$obj_pdo = new Conexion();
	        	$conexion = $obj_pdo->conectar();
	        	$sentencia = $conexion->prepare($sql);
	        	$sentencia->execute();
	        	$row = $sentencia->fetch();
	        	return $row;
        	}

        	catch (Exception $ex) {
        		die("Error: " . $ex->getMessage());
        	}
        }

        public function traerUno($value) {

        	try{
        		$sql = "SELECT * FROM " . $this->tabla . " WHERE Dni=" . $value;

	        	$obj_pdo = new Conexion();
	        	$conexion = $obj_pdo->conectar();
	        	$sentencia = $conexion->prepare($sql);
	        	$sentencia->execute();

				//var_dump($sentencia);
	        	while ($row = $sentencia->fetch()) {
	        		$array[] = $row;
	        	}
        	}

        	catch (Exception $ex) {
        		die("Error: " . $ex->getMessage());
        	}

        	if(!empty($array)){
        		return $array;

        	}

        }


        public function actualizar($value) {}

    }

    ?>