<?php namespace Daos;

use \Daos\Conexion as Conexion;

class VehiculoDao extends Conexion /*implements IDAO*/{
	protected $tabla = "Vehiculos";
	protected $listado;

	protected function mapear($gente)
	{
		$gente = is_array($gente) ? $gente : [];
		$this->listado = array_map(function($p){
			return new \Modelos\Usuario(isset($p['dominio']), isset($p['marca']), isset($p['modelo']), isset($p['fechaFab']), isset($p['dni']));
		}, $gente);
	}

	public function agregar($objeto) {

		try{
			$Dominio = $objeto->getDominio();
			$Marca = $objeto->getMarca();
			$Modelo = $objeto->getModelo();
			$FechaFab = $objeto->getFechaFab();
			$Dni = $objeto->getDni();


			$sql = "INSERT INTO $this->tabla  (Dominio, Marca, Modelo, FechaFab, Dni)  VALUES ('$Dominio', '$Marca', '$Modelo', '$FechaFab', '$Dni')";
	//var_dump($sql);

	//$sql = "INSERT INTO " . $this->tabla . " VALUES (:dni, :nombre, :fechaNacimiento, :domicilio, :password)";
	//echo $sql;

			$obj_pdo = new Conexion();

			$conexion = $obj_pdo->conectar();

			$sentencia = $conexion->prepare($sql);

	/*$sentencia->bindParam(":Dni", $Dni);
	$sentencia->bindParam(":Nombre", $Nombre);
    $sentencia->bindParam(":Apellido", $Apellido);
    $sentencia->bindParam(":Email", $Email);
    $sentencia->bindParam(":Domicilio", $Domicilio);
    $sentencia->bindParam(":Edad", $Edad);
    $sentencia->bindParam(":FechaNac", $FechaNac);*/

    //var_dump($sentencia);

	// Ejecuto la sentencia.
    $sentencia->execute();
}


catch (Exception $ex) {
	die("Error: " . $ex->getMessage());
}

}  	

public function eliminar($value) {

			// Guardo como string la consulta sql
	
	try{
		$sql = "DELETE FROM " . $this->tabla . " WHERE Dominio=" . "'" . $value . "'";
		// var_dump($sql);
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
		while ($row = $sentencia->fetch()) {
			$array[] = $row;
		}
	}


	catch (Exception $ex) {
		die("Error: " . $ex->getMessage());
	}

	if(!empty($array)){
		return $array;
	}

}
public function traerTodos() {

	
	try{
				// Guardo como string la consulta sql
		$sql = "SELECT * FROM " . $this->tabla;
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();

		while ($row = $sentencia->fetch()) {
			$array[] = $row;
		}
	}

	catch (Exception $ex) {
		die("Error: " . $ex->getMessage());
	}

	if(!empty($array)){
		return $array;
	}
}

public function traerCant(){
	
	try{
		$sql = "SELECT count(*) as Cant FROM " . $this->tabla;
			//var_dump($sql);
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
		$row = $sentencia->fetch();
		return $row;
	}

	

	catch (Exception $ex) {
		die("Error: " . $ex->getMessage());
	}
}

public function traerCantUser($Dni){
	
	try{
		$sql = "SELECT count(*) as Cant FROM " . $this->tabla . " WHERE ". "Dni=".$Dni;
			//var_dump($sql);
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
		$row = $sentencia->fetch();
		return $row;
	}

	

	catch (Exception $ex) {
		die("Error: " . $ex->getMessage());
	}
}

public function traerUno($value) {

	
	try{
		$sql = "SELECT * FROM Vehiculos WHERE Dominio = '$value' ";
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
		while ($row = $sentencia->fetch()) {
			$array[] = $row;
		}
	}

	catch (Exception $ex) {
		die("Error: " . $ex->getMessage());
	}

	if(!empty($array)){
		return $array;
	}
}
public function traerUnoDni($value) {

	try{
		$sql = "SELECT * FROM Vehiculos WHERE Dni = '$value' ";
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
		while ($row = $sentencia->fetch()) {
			$array[] = $row;
		}
	}

	catch (Exception $ex) {
		die("Error: " . $ex->getMessage());
	}

	if(!empty($array)){
		return $array;
	}
}



public function actualizar($value) {}

}

?>