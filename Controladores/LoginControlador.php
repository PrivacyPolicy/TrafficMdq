<?php namespace Controladores;

class LoginControlador{
    
    private $listado;
    
    public function __construct(){
    }
    
    public function loginUsuario($Dni, $Pass){
        /*echo $Dni;
        echo $Pass;*/
        $usuarios = new \Daos\UsuarioDao();
        
        $valides=$usuarios->traerUno($Dni);

        if($valides==NULL){
            echo 'Dni inexistente';
            //phpinfo();
        }
        else{
            $UserArray=$usuarios->isValid($Dni, $Pass);
            if($UserArray==NULL){
                echo 'Contraseña incorrecta';
                //require_once('Vistas/login.php');
            }
            else{
                //echo 'Bienvenido';
                $this->validarRolSession($Dni);
            }    
        }
    }

    public function validarRolSession($Dni){
        $usuarios = new \Daos\UsuarioDao();
        if($usuarios->isAdmin($Dni)){
            //echo ' Login Admin';
            $_SESSION['Dni'] = $Dni;
            $datos = new \Daos\TitularDao();
            $listadoCantT = $datos->traerCant(); //Cantidad de titulares para mostrar en la vista ppal
            
            $datosV = new \Daos\VehiculoDao();
            $listadoCantV = $datosV->traerCant(); //Cantidad de vehiculos para mostrar en la vista ppal
            
            $datosM = new \Daos\MultaDao();
            $listadoCantM = $datosM->traerCant(); //Cantidad de Multas para mostrar en la vista ppal

            
            require_once('Vistas/Admin/adminIndex.php');
            //include('Vistas/pagPrincipalAdmin.php');
        }
        else if($usuarios->isUser($Dni)){
            //echo ' Login User';
            $_SESSION['Dni'] = $Dni;

            $datosV = new \Daos\VehiculoDao();
            $listadoCantV = $datosV->traerCantUser($_SESSION['Dni']); //Cantidad de vehiculos para 
            $datosM = new \Daos\MultaDao();
            $listadoCantM = $datosM->traerCantUser($_SESSION['Dni']); //Cantidad de Multas para mostrar 
            require_once('Vistas/User/userIndex.php');
            //include('Vistas/pagPrincipalUser.php');

        }
    }

    public function cerrarSesion(){
        //end_sesion('Dni');
        include ('Vistas/login.php');  
    }

    public function vistaPpalAdmin(){

        $datosT = new \Daos\TitularDao();
        $listadoCantT = $datosT->traerCant(); //Cantidad de titulares para mostrar en la vista ppal
        $datosV = new \Daos\VehiculoDao();
        $listadoCantV = $datosV->traerCant(); //Cantidad de vehiculos para mostrar en la vista ppal
        $datosM = new \Daos\MultaDao();
        $listadoCantM = $datosM->traerCant(); //Cantidad de Multas para mostrar en la vista ppal
        
        require_once('Vistas/Admin/adminIndex.php');
    }
    public function vistaPpalUser(){
        
        
            $datosV = new \Daos\VehiculoDao();
            $listadoCantV = $datosV->traerCantUser($_SESSION['Dni']); //Cantidad de vehiculos para 
            $datosM = new \Daos\MultaDao();
            $listadoCantM = $datosM->traerCantUser($_SESSION['Dni']); //Cantidad de Multas para mostrar 
        require_once('Vistas/User/userIndex.php');
    }
    
    public function login(){

        include ('Vistas/login.php');  
    }
    

}
?>