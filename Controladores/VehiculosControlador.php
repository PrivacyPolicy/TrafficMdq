<?php namespace Controladores;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*require('../modelos/Vehiculo.php');
require('../daos/VehiculoJsonDAO.php');*/

class VehiculosControlador {

    private $value;

    public function __construct(){
    }

    public function vistaVehiculos(){
        $datos = new \Daos\VehiculoDao();
        $listadoCant = $datos->traerCant();
        $flag=0;
        require_once('Vistas/Admin/adminV.php');
    }

    public function vistaVehiculoUser(){
        $datos = new \Daos\VehiculoDao();
        $listado = $datos->traerUnoDni($_SESSION['Dni']);

        $flag=0;

        //echo $Dni;
        require_once ('Vistas/User/userV.php');
    }

    public function buscarVehiculo($dominio){
        $datos = new \Daos\VehiculoDao();
        $resultado = $datos->traerUno($dominio);
        $flag=1;
        
        require_once('Vistas/Admin/adminV.php');
    }
    public function buscarVehiculoDni($dni){
        $datos = new \Daos\VehiculoDao();
        $resultado = $datos->traerUnoDni($dni);
        $flag=1;
        
        require_once('Vistas/Admin/adminV.php');
    }

        public function agregarVehiculo($Dni,$Dominio,$FechaFab, $Marca, $Modelo){

        $datos = new \Daos\TitularDao();
        $try = $datos->traerUno($Dni);
        //echo 'dni';
        //var_dump($try);

        $vehiculo = new \Daos\VehiculoDao();
        //Llamo al titular dao para verificar que mi dni cargado en vehiculo pertenece a un titular
        //Utilizo trarUno, si me trae algo es porque encontro ese dni
        //var_dump($Dni);
        $verificacion_dominio=$vehiculo->traerUno($Dominio);

        if($try == !NULL && $verificacion_dominio == NULL)
        {   
            $objeto = new \Modelos\Vehiculo($Dominio,$Marca,$Modelo,$FechaFab,$Dni);
            //var_dump($objeto);
            $agregado = $vehiculo->agregar($objeto);
            $listado = $vehiculo->traerTodos();
            $listadoCant = $vehiculo->traerCant();
            $unicoVehiculo = $vehiculo->traerUno($Dominio);
            $flag=3;
            require_once ('Vistas/Admin/adminV.php'); 
        }
        else if($try == NULL)
        {
            $flag=4;
            //echo 'El dni no pertenece a un titular cargado en el sistema';
            require_once ('Vistas/Admin/adminV.php'); 
        }
        else
        {
            $flag=5;
            //echo 'Ya hay un vehiculo cargado con ese dominio';
            require_once ('Vistas/Admin/adminV.php'); 
        }
    }
    public function mostrarTodos(){
        $datos = new \Daos\VehiculoDao();
        $listadoCant = $datos->traerCant();
        $listado = $datos->traerTodos();
        $flag=6;
        //var_dump($listado);

        require_once('Vistas/Admin/adminV.php');
    }
    public function borrarVehiculo($dominio)
    {
        $datos= new \Daos\VehiculoDao();

        $listado = $datos->eliminar($dominio);
        $flag = 2;
        //echo $dominio;
        require_once('Vistas/Admin/adminV.php');
    }
}
?>