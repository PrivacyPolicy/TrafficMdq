<?php namespace Controladores;

class TitularesControlador{

	private $flag;

	public function __construct(){
	}

	public function Ejercicio(){
		require_once('Vistas/Ejercicios.php');
	}

	public function vistaTitulares(){
		$datos = new \Daos\TitularDao();
		$listadoCant = $datos->traerCant();
		$flag=0;

		require_once('Vistas/Admin/adminT.php');
	}



	public function vistaMultasUser(){

		
		require_once('Vistas/User/userM.php');
	}


	public function vistaTitularUser(){
		$datos = new \Daos\TitularDao();
		$listado = $datos->traerUno($_SESSION['Dni']);
		$flag=0;
		
		//echo $dni;
		//16074999

		require_once ('Vistas/User/userT.php');
	}

	public function buscarTitular($dni){
		$datos = new \Daos\TitularDao();
		$listado = $datos->traerUno($dni);
		//echo $dni;
		$flag=1;
		//36878817
		
		require_once('Vistas/Admin/adminT.php');
	}

	public function borrarTitular($dni){
		$datos = new \Daos\TitularDao();
		$listado = $datos->eliminar($dni);
		$flag=2;

		require_once('Vistas/Admin/adminT.php');
	}

	public function mostrarTodos(){
		$datos = new \Daos\TitularDao();
		$listadoCant = $datos->traerCant();
		$listado = $datos->traerTodos();
		$flag=5;
		//var_dump($listado);

		require_once('Vistas/Admin/adminT.php');
	}

	public function agregarTitular($Dni, $Nombre, $Apellido, $Email, $Domicilio, $Edad, $FechaNac){
		
		$datos = new \Daos\TitularDao();
		$try = $datos->traerUno($Dni);
		if($try==NULL){

			$objeto = new \Modelos\Titular($Dni, $Nombre, $Apellido, $Email, $Domicilio, $Edad, $FechaNac);
			$agregado = $datos->agregar($objeto);
			$listado = $datos->traerTodos();
			$listadoCant = $datos->traerCant();
			$flag=3;
			//titular creado con exito
			require_once ('Vistas/Admin/adminT.php');

		}
		else{
			$flag=4;
			//ese titular ya existe
			require_once ('Vistas/Admin/adminT.php');
		}

		
		 

		//require_once ('Vistas/Admin/adminT.php'); 

	}
}


?>