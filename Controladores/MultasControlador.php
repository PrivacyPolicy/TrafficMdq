<?php namespace Controladores;

class MultasControlador{

	private $flag;

	public function __construct(){
	}

	public function Ejercicio(){
		require_once('Vistas/Ejercicios.php');
	}



        public function vistaMultas(){
        $datos = new \Daos\MultaDao();
		$listadoCant = $datos->traerCant();
		$flag=0;

		require_once('Vistas/Admin/adminM.php');
	}


	public function vistaMultaUser(){
		$datos = new \Daos\MultaDao();
		$listado = $datos->traerMporDni($_SESSION['Dni']);
		$flag=0;
		//echo $dni;
		//16074999

		require_once ('Vistas/User/userM.php');
	}

	public function buscarMultaDni($dni){
		$datos = new \Daos\MultaDao();
		$listado = $datos->traerMporDni($dni);
		//echo $dni;
		$flag=1;
		//36878817
		
		require_once('Vistas/Admin/adminM.php');
	}

	public function buscarMultaDom($dominio){
		$datos = new \Daos\MultaDao();
		//var_dump($dominio);
		$listado = $datos->traerMporDominio($dominio);
		//echo $dominio;
		$flag=1;
		//36878817
		
		require_once('Vistas/Admin/adminM.php');
	}

	public function borrarMulta($dni){
		$datos = new \Daos\MultaDao();
		$listado = $datos->eliminar($dni);
		$flag=2;

		require_once('Vistas/Admin/adminM.php');
	}
	public function pagarMultaUser($dni){
		$datos = new \Daos\MultaDao();
		$listado = $datos->eliminar($dni);
		$flag=0;

		$listado = $datos->traerMporDni($_SESSION['Dni']);


		require_once('Vistas/User/userM.php');
	}

	public function mostrarTodos(){
		$datos = new \Daos\MultaDao();
		$listadoCant = $datos->traerCant();
		$listado = $datos->traerTodos();
		$flag=5;
		//var_dump($listado);

		require_once('Vistas/Admin/adminM.php');
	}

	public function agregarMulta($Dni, $Importe, $Fecha, $Dominio, $Detalle){
		
		$datos = new \Daos\MultaDao();
		$titular = new \Daos\TitularDao();
		$vehiculo = new \Daos\VehiculoDao();

		$verificacion = $datos->correspondencia($Dni, $Dominio);

		if($verificacion==!NULL){

			$objeto = new \Modelos\Multa($Dni, $Importe, $Fecha, $Dominio, $Detalle);
			$agregado = $datos->agregar($objeto);
			$listado = $datos->traerTodos();
			$listadoCant = $datos->traerCant();
			$flag=3;
			//titular creado con exito
			require_once ('Vistas/Admin/adminM.php');

		}
		else{
			$flag=4;
			//ese titular ya existe
			require_once ('Vistas/Admin/adminM.php');
		}

		
		 

		//require_once ('Vistas/Admin/adminT.php'); 

	}
}


?>