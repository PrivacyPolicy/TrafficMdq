<?php

session_start();

define('ROOT', __DIR__ . "/");
define('FRONT_ROOT', '');
define('FRONT', '/');

//define('ROOT', $_SERVER['REQUEST_URL'] . "/");
//define('ROOT', realpath(dirname(__FILE__))."/");
//define('URL', "http://c0220178.ferozo.com/grupo4/proyecto/");
//define('ROOT', $_SERVER['DOCUMENT_ROOT'] . '/grupo4/proyecto/');


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require "Config/Autoload.php";

Config\Autoload::iniciar();

$request = new Config\Request();

require_once ('Vistas/header.php');
$router = Config\Router::direccionar($request);
//require_once ('Vistas/footer.php');

?>