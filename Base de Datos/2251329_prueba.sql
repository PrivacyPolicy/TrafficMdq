-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: fdb15.biz.nf
-- Tiempo de generación: 11-05-2018 a las 01:31:56
-- Versión del servidor: 5.7.20-log
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `2251329_prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `superior` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Multas`
--

CREATE TABLE `Multas` (
  `IdMulta` int(11) NOT NULL,
  `Dni` int(11) NOT NULL,
  `Importe` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Dominio` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Detalle` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Multas`
--

INSERT INTO `Multas` (`IdMulta`, `Dni`, `Importe`, `Fecha`, `Dominio`, `Detalle`) VALUES
(1, 17943326, 8000, '2018-11-23', 'ert567', 'No se encontraba al dia la VTV.'),
(7, 1, 234234, '1234-11-11', 'rty456', 'Cruce en luz roja'),
(8, 17943326, 4000, '2017-11-23', 'ert567', 'Semaforo en Rojo.'),
(17, 1, 25000, '2018-03-08', 'RTY456', 'Exceso de Velocidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `nombre` varchar(30) NOT NULL,
  `stock` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Titulares`
--

CREATE TABLE `Titulares` (
  `Dni` int(11) NOT NULL,
  `Nombre` varchar(30) DEFAULT NULL,
  `Apellido` varchar(30) DEFAULT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Edad` int(11) DEFAULT NULL,
  `Domicilio` varchar(30) DEFAULT NULL,
  `FechaNac` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Titulares`
--

INSERT INTO `Titulares` (`Dni`, `Nombre`, `Apellido`, `Email`, `Edad`, `Domicilio`, `FechaNac`) VALUES
(1, 'aba', 'ok', 'asd@klm.com', 43, 'asd', '2000-12-11'),
(23142590, 'Ernest', 'Alexander', 'ealexander5@canalblog.com', 76, '5909 Kinsman Terrace', '1978-02-04'),
(17943326, 'Jose', 'Gonzales', 'jg@hotmail.com', 30, 'Luro 2600', '1987-12-11'),
(33304539, 'Aaron', 'Fields', 'afields8@hugedomains.com', 25, '2 Riverside Place', '1961-07-24'),
(29667432, 'Bobby', 'Martinez', 'bmartineza@whitehouse.gov', 58, '7654 Moland Crossing', '1996-05-18'),
(39625621, 'Bonnie', 'Murphy', 'bmurphyc@com.com', 18, '1161 Memorial Pass', '1965-01-21'),
(32919684, 'Brenda', 'Perry', 'bperryd@stumbleupon.com', 35, '3 Redwing Trail', '1965-06-08'),
(31742433, 'Amanda', 'Hunt', 'ahunte@google.de', 25, '03027 Northport Pass', '1975-12-04'),
(27138498, 'Catherine', 'Kennedy', 'ckennedyf@digg.com', 68, '84238 Summerview Center', '1970-04-19'),
(19236251, 'Helen', 'Barnes', 'hbarnesg@umich.edu', 22, '7495 Brown Park', '1994-09-28'),
(24063892, 'Sean', 'Sims', 'ssimsh@timesonline.co.uk', 60, '0227 Marquette Circle', '1951-04-08'),
(26201032, 'Arthur', 'Richardson', 'arichardsoni@epa.gov', 36, '32 Delladonna Court', '1977-03-19'),
(39708426, 'Sara', 'Ramos', 'sramosj@networksolutions.com', 17, '81 Brickson Park Center', '1994-03-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuarios`
--

CREATE TABLE `Usuarios` (
  `Dni` int(11) NOT NULL,
  `Pass` varchar(30) DEFAULT NULL,
  `Rol` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Usuarios`
--

INSERT INTO `Usuarios` (`Dni`, `Pass`, `Rol`) VALUES
(35320498, 'qwe123', 'admin'),
(17943326, 'qwe123', 'user'),
(39094271, 'qwe123', 'user'),
(19746261, 'qwe123', 'user'),
(25368452, 'qwe123', 'user'),
(17629920, 'qwe123', 'user'),
(23142590, 'qwe123', 'user'),
(26764228, 'qwe123', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Dni` int(11) NOT NULL,
  `Pass` varchar(30) DEFAULT NULL,
  `Rol` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Dni`, `Pass`, `Rol`) VALUES
(35320498, 'qwe123', 'admin'),
(17943326, 'qwe123', 'user'),
(39094271, 'qwe123', 'user'),
(19746261, 'qwe123', 'user'),
(25368452, 'qwe123', 'user'),
(17629920, 'qwe123', 'user'),
(23142590, 'qwe123', 'user'),
(26764228, 'qwe123', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Vehiculos`
--

CREATE TABLE `Vehiculos` (
  `Dni` int(11) DEFAULT NULL,
  `Dominio` varchar(8) NOT NULL,
  `Marca` varchar(30) DEFAULT NULL,
  `Modelo` varchar(30) DEFAULT NULL,
  `FechaFab` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Vehiculos`
--

INSERT INTO `Vehiculos` (`Dni`, `Dominio`, `Marca`, `Modelo`, `FechaFab`) VALUES
(17943326, 'Tre123', 'Mercedez', 'C300', '1993-11-11'),
(39094271, 'okg234', 'Mercedes Benz', 'C200', '2011-09-13'),
(19746261, 'plk876', 'Bmw', 'M3', '1999-08-24'),
(25368452, 'mnb834', 'Daihatsu', 'Cuore', '2001-02-09'),
(1, 'rty456', 'Renault', 'Falcon', '2012-12-24'),
(23142590, 'zkd503', 'Ford', 'Mondeo', '2009-03-30'),
(26764228, 'ols656', 'Audi', 'A4', '2003-07-14'),
(17943326, 'tyh719', 'Daihatsu', 'Cuore', '1993-02-14');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `Multas`
--
ALTER TABLE `Multas`
  ADD PRIMARY KEY (`IdMulta`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `Titulares`
--
ALTER TABLE `Titulares`
  ADD PRIMARY KEY (`Dni`);

--
-- Indices de la tabla `Usuarios`
--
ALTER TABLE `Usuarios`
  ADD PRIMARY KEY (`Dni`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Dni`);

--
-- Indices de la tabla `Vehiculos`
--
ALTER TABLE `Vehiculos`
  ADD PRIMARY KEY (`Dominio`),
  ADD KEY `Dni` (`Dni`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Multas`
--
ALTER TABLE `Multas`
  MODIFY `IdMulta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
